#!/bin/bash
mn -c;
python mptcp.py;
./plot_csv.py -i figure_4a.csv -o figure_4a -t 'Regular MPTCP' -x 'Receive Window (KB)' -y 'Throughput (Mbps)'
./plot_csv.py -i figure_4b.csv -o figure_4b -t 'Opportunistic Retransmit' -x 'Receive Window (KB)' -y 'Throughput (Mbps)'
./plot_csv.py -i figure_4c.csv -o figure_4c -t 'Opport. Retransmit+Penalizing' -x 'Receive Window (KB)' -y 'Throughput (Mbps)'
./plot_csv.py -i loss_quality.csv -o loss_quality -t 'MPTCP vs. TCP w/ WiFi loss' -x 'WiFi Loss Rate (%)' -y 'Throughput (Mbps)'
./plot_csv.py -i delay_quality.csv -o delay_quality -t 'MPTCP vs. TCP w/ Varying WiFi RTT' -x 'WiFi RTT (ms)' -y 'Throughput (Mbps)'
