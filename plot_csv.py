#!/usr/bin/python
import argparse
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

"""
Assumes at least two columns (one for x and one for y). Assumes that titles 
are in the first row of the input csv file.
"""

parser = argparse.ArgumentParser()
parser.add_argument('-o', '--out',
                    help="Output filename, e.g.: --out plot.png",
                    dest="out",
                    default=None)

parser.add_argument('-i', '--in',
                    help="Input csv filename (with path to file if necessary)",
                    dest="infile",
                    required=True)

parser.add_argument('-x', '--xlabel',
                    help="X axis label",
                    dest="xlabel",
                    default=None)

parser.add_argument('-y', '--ylabel',
                    help="Y axis label",
                    dest="ylabel",
                    default=None)

parser.add_argument('-t', '--title',
                    help="Plot title",
                    dest="title",
                    default=None)

args = parser.parse_args()

def parse_data(filename):
    lines = open(filename).read().split("\n")
    labels = lines[0].strip().strip(",").split(",")
    data = np.genfromtxt(filename, delimiter=",", skip_header=1, filling_values=0)
    return (labels, data)

def plot_line(x, y, label):
  plt.plot(x, y, label=label, lw=2)

print "Parsing input csv..."
labels, data = parse_data(args.infile)

print "Generating plot..."
plt.subplot(221)
xvalues = data[:,0]
for col in xrange(1, len(labels)):
  plot_line(xvalues, data[:,col], labels[col])

plt.legend(bbox_to_anchor=(1.05,1), loc=2, borderaxespad=0.)
plt.xlim(xmin=0)
plt.ylim(ymin=0)

if args.title:
  plt.title(args.title)
if args.xlabel:
  plt.xlabel(args.xlabel)
if args.ylabel:
  plt.ylabel(args.ylabel)

if args.out:
    print "Saving to %s" % args.out
    plt.savefig(args.out)
else:
    plt.show()
