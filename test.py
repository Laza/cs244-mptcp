#!/usr/bin/python

import boto.ec2
import socket
import time
import os
import subprocess
import sys
import datetime

temporaries = {}

# Initializes logging file to keep track of temporary stuff created
def log_changes():
  if temporaries:
    log_file = open ("undeleted_temporaries.txt", "w")
    for k, v in temporaries.iteritems():
      log_file.write ("%s:%s\n" % (k, v))
    log_file.close()
    print "Please manually remove the " + \
        "temporaries listed in undeleted_temporaries.txt"

# Gets localhost internet IP
def get_IP():
  s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  s.connect(("www.google.com",80))
  myIP = (s.getsockname()[0])
  s.close()
  return myIP

# Connects to the ec2 API
def connect_ec2():
  return boto.ec2.connect_to_region ("us-west-2",
      aws_access_key_id="AKIAJ2TYRUTAGHCAYDFQ",
      aws_secret_access_key="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")

# Creates a c3.large instance
def launch_c3_large(conn):
  global temporaries
  print "Launching an c3.large instance...",
  sys.stdout.flush()
  temporaries['created_instance'] = conn.run_instances(
      'ami-6ac2a85a',
      key_name='MPTCP_Experiment',
      instance_type='c3.large',
      security_groups=['MPTCP Experiment Security Group']).instances[0]
  print "[DONE]"

  # wait until the machine is up
  print "Waiting until created instance has booted...",
  sys.stdout.flush()
  while temporaries['created_instance'].state != 'running':
    time.sleep (5)
    temporaries['created_instance'].update()

  print "[DONE] instance-id:<%s>" % temporaries['created_instance'].id


def cleanup_instance(conn):
  if 'created_instance' in temporaries:
    print "Terminating <%s>..." % temporaries['created_instance'].id,
    sys.stdout.flush()
    conn.terminate_instances(temporaries['created_instance'].id)
    
    while temporaries['created_instance'].state != 'terminated':
      time.sleep (5)
      temporaries['created_instance'].update()
    del temporaries['created_instance']
    print "[DONE]"

# Creates a 20 GB EBS volume (to compile the kernel on)
def create_EBS_volume(conn):
  global temporaries
  print "Creating a 20GB EBS volume...",
  sys.stdout.flush()
  temporaries['created_volume'] = conn.create_volume(20, 'us-west-2c')
  print "[DONE] volume-id:<%s>" % temporaries['created_volume'].id

def cleanup_EBS_volume(conn):
  if 'created_volume' in temporaries:
    print "Deleting <%s>..." % temporaries['created_volume'],
    time.sleep (5)
    sys.stdout.flush()
    conn.delete_volume (temporaries['created_volume'].id)
    del temporaries['created_volume']
    print "[DONE]"

# Attached the created 20GB volume to /dev/sdi
# Note that on the machine itself this is called /dev/xvdi
def attach_EBS_volume(conn):
  print "Attaching volume-id:<%s> to instance-id:<%s>..." % \
      (temporaries['created_volume'].id, temporaries['created_instance'].id),
  sys.stdout.flush()
  conn.attach_volume(temporaries['created_volume'].id,
      temporaries['created_instance'].id, "/dev/sdf")
  print "[DONE]"

# Formats the attached volume (at /dev/sdf(xvdf on machine)/# )
# Then mounts it at /mnt/data
def format_mount_EBS_volume(conn):
  instance_ip = temporaries['created_instance'].public_dns_name

  print "Waiting for ssh service to come up...",
  sys.stdout.flush()
  ssh_retcode = 255
  while not ssh_retcode == 0:
    command = ("ssh " +
        "-o StrictHostKeyChecking=no " +
        "-i %s " % temporaries['created_keypair_path'] +
        "%s " % instance_ip + "'whoami'")
    process = subprocess.Popen (command, shell=True, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    process.communicate()
    ssh_retcode = process.returncode
    time.sleep (5)
  # Authorized keys might not yet be loaded
  time.sleep(5)
  print "[DONE]"

  # Format xvdf
  print "Formatting /dev/xvdf(/dev/sdf) [sudo mkfs.ext4 /dev/xvdf]...",
  sys.stdout.flush()
  command = ("ssh " +
      "-o StrictHostKeyChecking=no " +
      "-i %s " % temporaries['created_keypair_path'] +
      "%s " % instance_ip + "'sudo mkfs.ext4 /dev/xvdf'")
  process = subprocess.Popen (command, shell=True, stdout=subprocess.PIPE,
      stderr=subprocess.PIPE)
  process.communicate()
  if not process.returncode == 0:
    raise Exception("Disk format failed... :(")
  print "[DONE]"

  # Create mount point
  print "Creating mount point [mkdir -p /mnt/data]...",
  sys.stdout.flush()
  command = ("ssh " +
      "-o StrictHostKeyChecking=no " +
      "-i %s " % temporaries['created_keypair_path'] +
      "%s " % instance_ip + "'sudo mkdir -p /mnt/data'")
  process = subprocess.Popen (command, shell=True, stdout=subprocess.PIPE,
      stderr=subprocess.PIPE)
  process.communicate()
  if not process.returncode == 0:
    raise Exception("Mkdir failed... :(")
  print "[DONE]"

  # Mount xvdf
  print "Mounting /dev/xvdf(/dev/sdf) [" + \
      "sudo mount -t ext4 /dev/xvdf /mnt/data]...",
  sys.stdout.flush()
  command = ("ssh " +
      "-o StrictHostKeyChecking=no " +
      "-i %s " % temporaries['created_keypair_path'] +
      "%s " % instance_ip + "'sudo mount -t ext4 /dev/xvdf /mnt/data'")
  process = subprocess.Popen (command, shell=True, stdout=subprocess.PIPE,
      stderr=subprocess.PIPE)
  process.communicate()
  if not process.returncode == 0:
    raise Exception("Mount /dev/xvdf failed... :(((")
  # Wait for 5 seconds just in case the system's a little slow
  time.sleep (5)
  print "[DONE]"

  # Modify permissions so that we can scp into the drive
  print "Modifying mount point permissions " + \
      "[sudo chmod -R 777 /mnt/data]...",
  sys.stdout.flush()
  command = ("ssh " +
      "-o StrictHostKeyChecking=no " +
      "-i %s " % temporaries['created_keypair_path'] +
      "%s " % instance_ip + "'sudo chmod -R 777 /mnt/data'")
  process = subprocess.Popen (command, shell=True, stdout=subprocess.PIPE,
      stderr=subprocess.PIPE)
  process.communicate()
  if not process.returncode == 0:
    raise Exception("Chmod 777 /mnt/data failed...")
  print "[DONE]"


# Copy mptcp kernel tarball and apply patches
def prepare_kernel (conn):
  instance_ip = temporaries['created_instance'].public_dns_name

  # Copy tarball
  print "Copying mptcp_2014_05_17.tar.gz to instance " + \
    "[scp mptcp_2014_05_17.tar.gz <instance_ip>:/mnt/data/]...",
  sys.stdout.flush()
  command = ("scp " +
      "-o StrictHostKeyChecking=no " +
      "-i %s " % temporaries['created_keypair_path'] +
      "mptcp_2014_05_17.tar.gz " +
      "%s:/mnt/data/" % instance_ip)
  process = subprocess.Popen (command, shell=True, stdout=subprocess.PIPE,
      stderr=subprocess.PIPE)
  process.communicate()
  if not process.returncode == 0:
    raise Exception("Tarball copy failed...")
  print "[DONE]"

  # Untar
  print "Untaring " + \
    "[tar -xf /mnt/data/mptcp_2014_05_17.tar.gz -C /mnt/data/...]...",
  sys.stdout.flush()
  command = ("ssh " +
      "-o StrictHostKeyChecking=no " +
      "-i %s " % temporaries['created_keypair_path'] +
      "%s " % instance_ip + 
      "'tar -xf /mnt/data/mptcp_2014_05_17.tar.gz -C /mnt/data/'")
  process = subprocess.Popen (command, shell=True, stdout=subprocess.PIPE,
      stderr=subprocess.PIPE)
  process.communicate()
  if not process.returncode == 0:
    raise Exception("Untar failed...")
  print "[DONE]"


  # Install make, gcc, kernel-package, libncurses5-dev, fakeroot
  print "Installing packages [sudo apt-get -y install make " + \
    "gcc kernel-package libncurses5-dev fakeroot]...",
  sys.stdout.flush()
  command = ("ssh " +
      "-o StrictHostKeyChecking=no " +
      "-i %s " % temporaries['created_keypair_path'] +
      "%s " % instance_ip + 
      "'sudo apt-get -y install make gcc kernel-package " +
      "libncurses5-dev fakeroot'")
  process = subprocess.Popen (command, shell=True, stdout=subprocess.PIPE,
      stderr=subprocess.PIPE)
  process.communicate()
  if not process.returncode == 0:
    raise Exception("Installing packages failed...")
  print "[DONE]"

  # Apply patch to change /include/net/mptcp.h /net/mptcp/mptcp_output.c and
  # /net/mptcp/mptcp_ctrl.c

  # First copy the patches over (this also includes the kernel config patch
  # used later)
  print "Copying patch files " + \
    "[scp kernel_config.patch mptcp_optimizations_ctrl_vars.patch " + \
    "<instance_ip>:/mnt/data/mptcp_2014_05_17/]...",
  sys.stdout.flush()
  command = ("scp " +
      "-o StrictHostKeyChecking=no " +
      "-i %s " % temporaries['created_keypair_path'] +
      "kernel_config.patch mptcp_optimizations_ctrl_vars.patch " +
      "%s:/mnt/data/mptcp_2014_05_17/" % instance_ip)
  process = subprocess.Popen (command, shell=True, stdout=subprocess.PIPE,
      stderr=subprocess.PIPE)
  process.communicate()
  if not process.returncode == 0:
    raise Exception("Patch copy failed...")
  print "[DONE]"

  # Apply mptcp_optimizations_ctrl_var.patch
  print "Applying mptcp_optimizations_ctrl_vars.patch " + \
    "[patch -d /mnt/data/mptcp_2014_05_17/ -p1 < " + \
    "/mnt/data/mptcp_2014_05_17/mptcp_optimizations_ctrl_vars.patch]...",
  sys.stdout.flush()
  command = ("ssh " +
      "-o StrictHostKeyChecking=no " +
      "-i %s " % temporaries['created_keypair_path'] +
      "%s " % instance_ip + 
      "'patch -d /mnt/data/mptcp_2014_05_17/ -p1 < " +
      "/mnt/data/mptcp_2014_05_17/mptcp_optimizations_ctrl_vars.patch'")
  process = subprocess.Popen (command, shell=True, stdout=subprocess.PIPE,
      stderr=subprocess.PIPE)
  process.communicate()
  if not process.returncode == 0:
    raise Exception("mptcp_optimizations_ctrl_var.patch application failed...")
  print "[DONE]"

  # Copy the kernel .config file that is currently being used by the instance
  # over
  print "Copying machine's kernel .config file to mptcp directory " + \
    "[cp /boot/config-3.13.0-24-generic " + \
    "/mnt/data/mptcp_2014_05_17/.config]...",
  sys.stdout.flush()
  command = ("ssh " +
      "-o StrictHostKeyChecking=no " +
      "-i %s " % temporaries['created_keypair_path'] +
      "%s " % instance_ip + 
      "'cp /boot/config-3.13.0-24-generic " +
      "/mnt/data/mptcp_2014_05_17/.config'")
  process = subprocess.Popen (command, shell=True, stdout=subprocess.PIPE,
      stderr=subprocess.PIPE)
  process.communicate()
  if not process.returncode == 0:
    raise Exception("Old kernel .config copy failed...")
  print "[DONE]"

  # Do a make config, disabling all options
  print "Doing a make oldconfig (all options disabled) " + \
    '[yes "" | make -C /mnt/data/mptcp_2014_05_17/ oldconfig]...',
  sys.stdout.flush()
  command = ("ssh " +
      "-o StrictHostKeyChecking=no " +
      "-i %s " % temporaries['created_keypair_path'] +
      "%s " % instance_ip + 
      "\"yes '' | make -C /mnt/data/mptcp_2014_05_17/ oldconfig\"")
  process = subprocess.Popen (command, shell=True, stdout=subprocess.PIPE,
      stderr=subprocess.PIPE)
  process.communicate()
  if not process.returncode == 0:
    raise Exception("Make oldconfig failed...")
  print "[DONE]"

  # Applying .config patch
  print "Applying kernel_config.patch " + \
    "[patch -d /mnt/data/mptcp_2014_05_17/ -p1 < " + \
    "/mnt/data/mptcp_2014_05_17/kernel_config.patch]...",
  sys.stdout.flush()
  command = ("ssh " +
      "-o StrictHostKeyChecking=no " +
      "-i %s " % temporaries['created_keypair_path'] +
      "%s " % instance_ip + 
      "'patch -d /mnt/data/mptcp_2014_05_17/ -p1 < " +
      "/mnt/data/mptcp_2014_05_17/kernel_config.patch'")
  process = subprocess.Popen (command, shell=True, stdout=subprocess.PIPE,
      stderr=subprocess.PIPE)
  process.communicate()
  if not process.returncode == 0:
    raise Exception("kernel_config.patch application failed...")
  print "[DONE]"

def install_kernel(conn):
  instance_ip = temporaries['created_instance'].public_dns_name

  # Make the kernel
  print "Making kernel [cd /mnt/data/mptcp_2014_05_17/ && " + \
    "sudo make -j3; sudo make-kpkg clean; " + \
    "sudo fakeroot make-kpkg --initrd " + \
    "--append-to-version=-mptcp kernel-image kernel-headers]..."
  print "This will take a while (~ 5 Hours)."
  print "System time at start:" + str(datetime.datetime.now())
  sys.stdout.flush()
  command = ("ssh " +
      "-o StrictHostKeyChecking=no " +
      "-i %s " % temporaries['created_keypair_path'] +
      "%s " % instance_ip + 
      "'/bin/sh -c \"cd /mnt/data/mptcp_2014_05_17/ && " +
      "sudo make -j3; sudo make-kpkg clean; " +
      "sudo fakeroot make-kpkg --initrd " +
      "--append-to-version=-mptcp kernel-image kernel-headers\"'")
  process = subprocess.Popen (command, shell=True, stdout=subprocess.PIPE,
      stderr=subprocess.PIPE)
  std, err = process.communicate()
  if not process.returncode == 0:
    print std, err
    raise Exception("Make kernel failed...")
  print "[DONE]"

  # Install the .deb files located at
  # /mnt/data/linux-headers-3.14.0-mptcp_3.14.0-mptcp-10.00.Custom_amd64.deb
  # /mnt/data/linux-image-3.14.0-mptcp_3.14.0-mptcp-10.00.Custom_amd64.deb
#sudo dpkg -i
#/mnt/data/linux-headers-3.14.0-mptcp_3.14.0-mptcp-10.00.Custom_amd64.deb
#/mnt/data/linux-image-3.14.0-mptcp_3.14.0-mptcp-10.00.Custom_amd64.deb 

  # Add lines to /boot/grub/menu.lst
# After           ## ## End Default Options ##
# title           Ubuntu 14.04 LTS, kernel 3.13.0-24-generic
# root            (hd0)
# kernel          /boot/vmlinuz-3.14.0-mptcp root=LABEL=cloudimg-rootfs ro
# console=hvc0
# initrd          /boot/initrd.img-3.14.0-mptcp

# Then create snapshot of volume
# Create AMI
# launch AMI


# Creates a security group with port 22 open to Any IP
def create_security_group(conn):
  global temporaries
  print "Creating security group <'MPTCP Experiment Security Group'>...",
  sys.stdout.flush()
  myIP = get_IP()
  temporaries['created_security_group'] = conn.create_security_group(
      "MPTCP Experiment Security Group", "Port 22 open to %s" % myIP)
  print "[DONE]"

  print "Adding rule: port 22, ip <%s> to " % myIP + \
      "<'MPTCP Experiment Security Group'>...",
  sys.stdout.flush()
  # Open up port 22 to myIP
  conn.authorize_security_group("MPTCP Experiment Security Group",
      ip_protocol="tcp", from_port=22, to_port=22, cidr_ip="%s/32" % myIP)
  print "[DONE]"

def cleanup_security_group(conn):
  if 'created_security_group' in temporaries:
    print "Deleting security group " + \
        "<'MPTCP Experiment Security Group'>...",
    sys.stdout.flush()
    temporaries['created_security_group'].delete()
    del temporaries['created_security_group']
    print "[DONE]"

# Creates a Keypair named 'MPTCP Experiment' if one doesn't already exist
def create_keypair(conn):
  global temporaries
  print "Creating keypair <'MPTCP_Experiment'>...",
  sys.stdout.flush()
  temporaries['created_keypair'] = conn.create_key_pair("MPTCP_Experiment")
  print "[DONE]"
  save_keypair(conn)

# Saves keypair to '.' directory
def save_keypair(conn):
  global temporaries
  created_keypair_path = os.getcwd()

  created_keypair_path += '/' + \
      str(temporaries['created_keypair']).split(':')[1] + '.pem'

  print "Saving keypair <'MPTCP_Experiment'> to %s..." % created_keypair_path,
  sys.stdout.flush()
  # Make sure the keypair file doesn't already exist
  try:
    os.remove (created_keypair_path)
  except OSError:
    pass

  temporaries['created_keypair'].save(os.getcwd())
  temporaries['created_keypair_path'] = created_keypair_path.strip()
  print "[DONE]"

def cleanup_keypair(conn):
  if 'created_keypair' in temporaries:
    print "Deleting keypair <'MPTCP_Experiment'>...",
    sys.stdout.flush()
    temporaries['created_keypair'].delete()
    del temporaries['created_keypair']
    print "[DONE]"
  if 'created_keypair_path' in temporaries:
    print "Removing keypair file %s..." % temporaries['created_keypair_path'],
    sys.stdout.flush()
    os.remove (temporaries['created_keypair_path'])
    del temporaries['created_keypair_path']
    print "[DONE]"

def main():
  print get_IP()
  conn = connect_ec2()
  create_keypair(conn)
  create_security_group(conn)
  create_EBS_volume(conn)
  launch_c3_large(conn)
  attach_EBS_volume(conn)
  format_mount_EBS_volume(conn)
  prepare_kernel (conn)
  install_kernel (conn)

  raw_input ("Enter to continue...")

  cleanup_instance(conn)
  cleanup_EBS_volume(conn)
  cleanup_security_group(conn)
  cleanup_keypair(conn)
  log_changes()

if __name__ == "__main__":
  try:
    try:
      main()
    except:
      print "\n\nEXCEPTION!!!\n\n"
      print "Cleaning up."
      conn = connect_ec2()
      cleanup_instance(conn)
      cleanup_EBS_volume(conn)
      cleanup_security_group(conn)
      cleanup_keypair(conn)
      log_changes()
      raise
  except:
    log_changes()
    raise
