#!/usr/bin/python

from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.util import dumpNodeConnections
from mininet.cli import CLI
from mininet.node import OVSController

import subprocess
from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process

import sys
import os
import math

# IP Addresses for the sender (h1) and receiver (h2) interfaces
# These are HARD CODED into the begin_mptcp_experiment function. 
# DO NOT CHANGE - HARD CODED
sender_wifi_interface_ip = '10.0.0.3'
sender_3g_interface_ip = '10.0.1.3'

receiver_wifi_interface_ip = '10.0.0.4'
# For some odd reason, you must establish an iperf connection to the 3g
# interface in order for mptcp to get going. If you connect to the wifi
# interface the mptcp connection doesn't detect the other interface (3g) for
# some reason (???)
receiver_3g_interface_ip = '10.0.1.4'

wifi_throughput_interface = 's0-eth2'
threeg_throughput_interface = 's1-eth2'
# DO NOT CHANGE - HARD CODED


# Experiment x axis
receive_window_min = 5
receive_window_max = 1200
receive_window_step = 5

# Experiment control vars
iperf_run_time = 3600
warm_up_time_min = 5
wait_slope = 0.00
NUM_TRIES = 3

# Topology to be instantiated in Mininet
class MyTopo(Topo):
    '''
       |---- s0 -- WiFi Link ---|
      h1                       h2
       |---- s1 -- 3G Link -----|
    '''
    def __init__(self, wifiLoss=0, wifiDelay=10):
        super(MyTopo, self ).__init__()
        self.create_topology(wifiLoss, '%dms' % wifiDelay)

    def create_topology(self, wifiLoss, wifiDelay):
        sender = self.addHost("h1")
        receiver = self.addHost("h2")
        switchWifi = self.addSwitch ("s0")
        switch3G = self.addSwitch ("s1")

        # Add wifi link
        # Characteristics given in page 8 of How Hard Can It Be?
        # BW 8Mbps, 20ms RTT
        # FORMAT: small # big
        wifiQSize = math.ceil((8*(1000000/8.0)*0.02)/1500) # ((8000000/8.0)*0.08)/1500
        self.addLink(switchWifi, sender, bw=100, delay='0.01ms', max_queue_size=wifiQSize)
        self.addLink(switchWifi, receiver, bw=8, delay=wifiDelay, max_queue_size=wifiQSize, loss=wifiLoss)

        # Add 3G link
        # Characteristics given in page 8 of How Hard Can It Be?
        # BW 2Mbps, 150ms RTT
        threeGQSize = math.ceil((2*(1000000/8.0)*0.15)/1500) # ((2000000/8.0)*2)/1500
        self.addLink(switch3G, sender, bw=100, delay='0.01ms', max_queue_size=threeGQSize)
        self.addLink(switch3G, receiver, bw=2, delay='75ms', max_queue_size=threeGQSize)

"Code from here to line 147 are copied from cs244, pa2"

def median(l):
    "Compute median from an unsorted list of values"
    s = sorted(l)
    if len(s) % 2 == 1:
        return s[(len(l) + 1) / 2 - 1]
    else:
        lower = s[len(l) / 2 - 1]
        upper = s[len(l) / 2]
        return float(lower + upper) / 2

def get_txbytes(iface):
    f = open('/proc/net/dev', 'r')
    lines = f.readlines()
    for line in lines:
        if iface in line:
            break
    f.close()
    if not line:
        raise Exception("could not find iface %s in /proc/net/dev:%s" %
                        (iface, lines))
    return float(line.split()[9])

def get_rates(iface, nsamples=3, period=1.0, wait=1.0):
    """Returns the interface @iface's current utilization in Mb/s.  It
    returns @nsamples samples, and each sample is the average
    utilization measured over @period time.  Before measuring it waits
    for @wait seconds to 'warm up'."""
    # Returning nsamples requires one extra to start the timer.
    nsamples += 1
    last_time = 0
    last_txbytes = 0
    ret = []
    sleep(wait)
    while nsamples:
        nsamples -= 1
        txbytes = get_txbytes(iface)
        now = time()
        elapsed = now - last_time
        #if last_time:
        #    print "elapsed: %0.4f" % (now - last_time)
        last_time = now
        # Get rate in Mbps; correct for elapsed time.
        rate = (txbytes - last_txbytes) * 8.0 / 1e6 / elapsed
        if last_txbytes != 0:
            # Wait for 1 second sample
            ret.append(rate)
        last_txbytes = txbytes
        sys.stdout.flush()
        sleep(period)
    return ret

"End copied code"

def reset_sysctl_options():
    set_sysctl('net.mptcp.mptcp_enabled', 1)
    set_sysctl('net.mptcp.mptcp_opportunistic_retransmit', '1')
    set_sysctl('net.mptcp.mptcp_penalize_slow_subflows', '1')
    set_sysctl('net.ipv4.tcp_rmem', '10240 87380 16777216')

# Gets the value of sysctl variable var
def get_sysctl(var):
    return subprocess.Popen (["sysctl", var],
        stdout=subprocess.PIPE).communicate()[0].split()

# Sets the value of sysctl variable var to value
def set_sysctl(var, value):
    subprocess.Popen (["sysctl", "-w", "%s=%s" % (var, value)],
        stdout=subprocess.PIPE).communicate()

def verify_topology(net):
    verify_latency(net)
    verify_bandwidth(net)

# Given 5 output lines of 'ping', calculate the average latency
def calc_average_latency (pingOutputLines):
    total = 0
    for line in pingOutputLines:
      ping = float(line.split("time=")[1].split()[0])
      total += ping

    return total / 5.0

def verify_latency(net):
    print "Verifying the latency of topology:"
    h1 = net.getNodeByName ("h1")
    h2 = net.getNodeByName ("h2")
    wifiRTT = h1.popen("ping %s -i 0.2 -c 6" % 
        (receiver_wifi_interface_ip)).communicate()[0].split('\n')[2:7]
    print "Wifi link RTT (20ms expected)"
    print calc_average_latency (wifiRTT)

    threeGRTT = h1.popen("ping %s -i 0.2 -c 6" % 
        (receiver_3g_interface_ip)).communicate()[0].split('\n')[2:7]
    print "3G link RTT (150ms expected)"
    print calc_average_latency (threeGRTT)

def verify_bandwidth(net):
    print "Verifying the bandwidth of topology (takes around 30 seconds):"
    h1 = net.getNodeByName ("h1")
    h2 = net.getNodeByName ("h2")

    # Wifi test
    h2.popen ("iperf -s -w 16m -p 5050")
    h1.popen ("iperf -p 5050 -c %s -t 20" % (receiver_wifi_interface_ip))
    print "Wifi link bandwidth (8 expected)"
    wifi = get_rates (wifi_throughput_interface)
    print median(wifi)

    stop_iperf()

    # 3G test
    h2.popen ("iperf -s -w 16m -p 5050")
    h1.popen ("iperf -p 5050 -c %s -t 20" % (receiver_3g_interface_ip))
    print "3G link bandwidth (2 expected)"
    threeg = get_rates (threeg_throughput_interface)
    print median(threeg)

    stop_iperf()

def start_receiver(net, options = "-i 1"):
    receiver = net.getNodeByName ("h2")

    return receiver.popen ("iperf -s -f k %s" % (options), shell=True)

def start_sender(net, receiver_ip, run_time=iperf_run_time):
    sender = net.getNodeByName ("h1")
    return sender.popen ("iperf -c %s -f k -t %d -i 1" %
        (receiver_ip, run_time), shell=True)

def stop_iperf ():
    os.system('killall -9 iperf')
    sleep(1)

def recordNetstat (net, rec, tryNum):
    h1 = net.getNodeByName ("h1")
    h2 = net.getNodeByName ("h2")
    h1.popen ("netstat > senderNetstatTry%sRcv%s" % 
        (tryNum, rec), shell=True)

def printMeasuredThroughput (net, wifi, threeg):
    print "Wifi interface measured throughput"
    print wifi
    print "3G interface measured throughput"
    print threeg


# Data parameter format:
# (Array of tuples)
# [ (Column 1 Name, [values]), (Column 2 Name, [values]), ... ]
# E.g.
# [ ("Rcv window size", [50, 100, 150]), 
#       ("TCP over WIFI throughput", [1.22, 2.343, 5.33]) ]
# Would result in a csv containing
# Rcv window size, TCP over WIFI throughput
# 50,1.22
# 100,2.343
# 150,5.33
def write_csv(filename, data):
    out = open(filename, "w")

    # Titles and sanity check data
    length = -1
    for column in data:
      out.write('%s,' % column[0])
      if not length == -1:
        if not length == len(column[1]):
          print "---CSV OUTPUT ERROR: BAD DATA FORMAT (columns diff size)---"
          return
      else:
        length = len(column[1])
    out.write ('\n')

    # Write out the columns
    for x in xrange(length):
      for column in data:
        out.write('%s,' % str(column[1][x]))
      out.write ('\n')

    out.close()

def extract_iperf_server_goodput_output(filename):
    iperfOutFile = open (filename, "r")

    # Discard the first six lines
    iperfOutFile.readline()
    iperfOutFile.readline()
    iperfOutFile.readline()
    iperfOutFile.readline()
    iperfOutFile.readline()
    iperfOutFile.readline()

    # Discard warmup lines
    for x in xrange (warm_up_time_min):
      iperfOutFile.readline()

    accumulation = 0
    count = 0

    for line in iperfOutFile.readlines():
      # Always in Kbits/s since we set -f k in start_receiver
      accumulation += float(line.split()[-2])
      count += 1

    iperfOutFile.close()

    # Convert to Mbits/s and return
    return (accumulation / count) / 1024.0

def begin_mptcp_experiment(net):
    h1 = net.getNodeByName ("h1")
    h2 = net.getNodeByName ("h2")

    # Manually set the ip address of the interfaces
    h1.cmdPrint('ifconfig h1-eth0 10.0.0.3 netmask 255.255.255.0')
    h2.cmdPrint('ifconfig h2-eth0 10.0.0.4 netmask 255.255.255.0')

    h1.cmdPrint('ifconfig h1-eth1 10.0.1.3 netmask 255.255.255.0')
    h2.cmdPrint('ifconfig h2-eth1 10.0.1.4 netmask 255.255.255.0')

    # Ensure that h1 routes the packets through the right interface (not
    # required but good to have it anyway - if these rules weren't set then h1
    # would try to open a connection to 10.0.1.4 from 10.0.0.3, which mininet
    # would block anyway since there is no link)
    dev = 'h1-eth0'
    table = 1
    h1.cmdPrint('ip rule add from 10.0.0.3 table %s' % (table))
    h1.cmdPrint('ip route add 10.0.0.0/24 dev %s scope link table %s' % (dev, table))
    h1.cmdPrint('ip route add default via 10.0.0.1 dev %s table %s' % (dev, table))

    dev = 'h1-eth1'
    table = 2
    h1.cmdPrint('ip rule add from 10.0.1.3 table %s' % (table))
    h1.cmdPrint('ip route add 10.0.1.0/24 dev %s scope link table %s' % (dev, table))
    h1.cmdPrint('ip route add default via 10.0.1.1 dev %s table %s' % (dev, table))

# Receive window size and tcp over wifi throughput, produce in figure_4a() 
# is global since it is reused in figures 4b and 4c
rcv_window = []
tcpWifiThroughput = []

def figure_4a (net):
    # Disable MPTCP
    set_sysctl('net.mptcp.mptcp_enabled', '0')

    threeGThroughput = [] 
    regularMPTCPThroughput = []

    # TCP over WiFi and 3G
    for receive_window_size in xrange(receive_window_min, receive_window_max, 
        receive_window_step):

      # Only have to create this list once here, no need to do it again in
      # 'Regular MPTCP' result gen
      rcv_window.append (receive_window_size)

      rwKb = receive_window_size * 1024
      set_sysctl('net.ipv4.tcp_rmem', '%s %s %s' % (rwKb, rwKb, rwKb))

      receiveProcess = start_receiver (net)
      sendProcess = start_sender (net, receiver_wifi_interface_ip, 3600)
      sendProcess = start_sender (net, receiver_3g_interface_ip, 3600)

      # For regular TCP the wait time can be very short
      sleep (5)

      print "Wifi throughput at " + str(receive_window_size)
      a = median (get_rates('s0-eth2'))
      print a
      tcpWifiThroughput.append(a)
      print "3G throughput at " + str(receive_window_size)
      a = median (get_rates('s1-eth2'))
      print a
      threeGThroughput.append(a)

      stop_iperf()

    # Enable MPTCP
    set_sysctl('net.mptcp.mptcp_enabled', '1')
    # Disable optimizations: opportunistic retransmit and penalizing
    set_sysctl('net.mptcp.mptcp_opportunistic_retransmit', '0')
    set_sysctl('net.mptcp.mptcp_penalize_slow_subflows', '0')

    # 'Regular MPTCP'
    for receive_window_size in xrange(receive_window_min, receive_window_max, 
        receive_window_step):

      rwKb = receive_window_size * 1024
      set_sysctl('net.ipv4.tcp_rmem', '%s %s %s' % (rwKb, rwKb, rwKb))

      collected_throughputs = []
      for i in xrange(0,NUM_TRIES):
        # Note that receiver interface ip doesn't matter since MPTCP
        # will find both interfaces anyway!
        receiveProcess = start_receiver (net)
        sendProcess = start_sender (net, receiver_3g_interface_ip, 3600)

        # Voodoo equation to ensure that MPTCP ramps up throughput
        sleep (max(warm_up_time_min, int(wait_slope * receive_window_size)))

        wifi = median(get_rates('s0-eth2'))
        threeg =  median(get_rates('s1-eth2'))
        #printMeasuredThroughput (net, wifi, threeg)
        throughput = wifi + threeg
        collected_throughputs.append(throughput)
        #recordNetstat (net, receive_window_size, i)
        stop_iperf()

      print "Regular MPTCP throughput at " + str(receive_window_size)
      print collected_throughputs
      med_throughput = median(collected_throughputs)
      print med_throughput
      regularMPTCPThroughput.append(med_throughput)

    write_csv ("figure_4a.csv", [("Receive window size (KB)", rcv_window),
        ("TCP over WiFi", tcpWifiThroughput),
        ("TCP over 3G", threeGThroughput),
        ("Regular MPTCP", regularMPTCPThroughput)])

def figure_4b (net):
    # Enable MPTCP
    set_sysctl('net.mptcp.mptcp_enabled', '1')
    # Enable optimization: opportunistic retransmit
    set_sysctl('net.mptcp.mptcp_opportunistic_retransmit', '1')
    # Disable optimization: penalizing
    set_sysctl('net.mptcp.mptcp_penalize_slow_subflows', '0')

    m1Throughput = []
    m1Goodput = []

    # 'MPTCP with M1, goodput and throughput'
    for receive_window_size in xrange(receive_window_min, receive_window_max, 
        receive_window_step):

      rwKb = receive_window_size * 1024
      set_sysctl('net.ipv4.tcp_rmem', '%s %s %s' % (rwKb, rwKb, rwKb))

      collected_throughputs = []
      collected_goodputs = []
      for i in xrange(0,NUM_TRIES):
        # Note that receiver interface ip doesn't matter since MPTCP
        # will find both interfaces anyway!
        receiveProcess = start_receiver (net, 
            "-i 1 -y > 4b_" + str(receive_window_size))
        sendProcess = start_sender (net, receiver_3g_interface_ip, 3600)

        # Voodoo equation to ensure that MPTCP ramps us throughput
        sleep (max(warm_up_time_min, int(wait_slope * receive_window_size)))
        wifi = median(get_rates('s0-eth2'))
        threeg =  median(get_rates('s1-eth2'))
        #printMeasuredThroughput (net, wifi, threeg)
        throughput = wifi + threeg
        collected_throughputs.append(throughput)

        stop_iperf()

        # Parse the results of iperf runs and get the goodput
        collected_goodputs.append (extract_iperf_server_goodput_output ("4b_" +
              str(receive_window_size)))

        # Cleanup iperf pipe file
        os.system ('rm -f 4b_' + str(receive_window_size))

      print "MPTCP + O1 throughput at " + str(receive_window_size)
      print collected_throughputs
      med_throughput = median(collected_throughputs)
      print med_throughput
      m1Throughput.append(med_throughput)
      print "MPTCP + O1 goodput at " + str(receive_window_size)
      print collected_goodputs
      med_goodput = median(collected_goodputs)
      print med_goodput
      m1Goodput.append(med_goodput)

    write_csv ("figure_4b.csv", [("Receive window size (KB)", rcv_window),
        ("TCP over WiFi", tcpWifiThroughput),
        ("MPTCP+M1 Throughput", m1Throughput),
        ("MPTCP+M1 Goodput", m1Goodput)])

def figure_4c (net):
    # Enable MPTCP
    set_sysctl('net.mptcp.mptcp_enabled', '1')
    # Enable optimizations: opportunistic retransmit, penalizing
    set_sysctl('net.mptcp.mptcp_opportunistic_retransmit', '1')
    set_sysctl('net.mptcp.mptcp_penalize_slow_subflows', '1')

    m12Throughput = []
    m12Goodput = []

    # 'MPTCP with M1 and M2 goodput'
    for receive_window_size in xrange(receive_window_min, receive_window_max, 
        receive_window_step):

      rwKb = receive_window_size * 1024
      set_sysctl('net.ipv4.tcp_rmem', '%s %s %s' % (rwKb, rwKb, rwKb))

      collected_throughputs = []
      collected_goodputs = []
      for i in xrange(0,NUM_TRIES):
        # Note that receiver interface ip doesn't matter since MPTCP
        # will find both interfaces anyway!
        receiveProcess = start_receiver (net, 
            "-i 1 -y > 4c_" + str(receive_window_size))
        sendProcess = start_sender (net, receiver_3g_interface_ip, 3600)
        sleep (warm_up_time_min)
        wifi = median(get_rates('s0-eth2'))
        threeg =  median(get_rates('s1-eth2'))
        #printMeasuredThroughput (net, wifi, threeg)
        throughput = wifi + threeg
        collected_throughputs.append(throughput)
        stop_iperf()

        # Parse the results of iperf runs and get the goodput
        collected_goodputs.append (extract_iperf_server_goodput_output ("4c_" +
              str(receive_window_size)))

        # Cleanup iperf pipe file
        os.system ('rm -f 4c_' + str(receive_window_size))

      print "MPTCP + O1 + O2 throughput at " + str(receive_window_size)
      print collected_throughputs
      med_throughput = median(collected_throughputs)
      print med_throughput
      m12Throughput.append(med_throughput)
      print "MPTCP + O1 + O2 goodput at " + str(receive_window_size)
      print collected_goodputs
      med_goodput = median(collected_goodputs)
      print med_goodput
      m12Goodput.append(med_goodput)

    write_csv ("figure_4c.csv", [("Receive window size (KB)", rcv_window),
        ("TCP over WiFi", tcpWifiThroughput),
        ("MPTCP+M1+M2 Throughput", m12Throughput),
        ("MPTCP+M1+M2 Goodput", m12Goodput)])

# Varies delay on the Wifi link
def runRTTExp():
    print "Running RTT experiment:"
    delays = range(10,110,5)
    tcpWifiThroughput_delay = []
    tcpAggThroughput_delay = []
    mptcpThroughput_delay = []
    for delay in delays:
      # Create topology with given delay
      topo = MyTopo(wifiDelay=delay)
      net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink, controller=OVSController)
      net.start()
      begin_mptcp_experiment(net)

      # Do regular TCP throughput measurements
      receiveProcess = start_receiver(net)
      sendProcess = start_sender(net, receiver_wifi_interface_ip, 3600)
      sendProcess = start_sender(net, receiver_3g_interface_ip, 3600)
      sleep(5)
      print "Wifi throughput with RTT " + str(delay*2)
      a = median(get_rates('s0-eth2'))
      print a
      tcpWifiThroughput_delay.append(a)
      print "3G throughput"
      b = median(get_rates('s1-eth2'))
      print b
      print "Aggregate TCP throughput with WiFi RTT " + str(delay*2)
      c = a+b
      print c
      tcpAggThroughput_delay.append(c)

      stop_iperf()

      # Enable MPTCP with optimizations
      set_sysctl('net.mptcp.mptcp_enabled', '1')
      set_sysctl('net.mptcp.mptcp_opportunistic_retransmit', '1')
      set_sysctl('net.mptcp.mptcp_penalize_slow_subflows', '1')

      # Do MPTCP throughput measurements
      collected_throughputs = []
      for i in xrange(0, NUM_TRIES):
        receiveProcess = start_receiver(net)
        sendProcess = start_sender(net, receiver_3g_interface_ip, 3600)
        sleep(warm_up_time_min)
        wifi = median(get_rates('s0-eth2'))
        threeg = median(get_rates('s1-eth2'))
        collected_throughputs.append(wifi + threeg)
        stop_iperf()

      print "MPTCP throughput with WiFi RTT " + str(delay*2)
      print collected_throughputs
      med_throughput = median(collected_throughputs)
      print med_throughput
      mptcpThroughput_delay.append(med_throughput)

      # Disable MPTCP and optimizations
      set_sysctl('net.mptcp.mptcp_enabled', '0')
      set_sysctl('net.mptcp.mptcp_opportunistic_retransmit', '0')
      set_sysctl('net.mptcp.mptcp_penalize_slow_subflows', '0')

      # Stop the network
      net.stop()

    write_csv ("delay_quality.csv", [("WiFi RTT (ms)", [x*2 for x in delays]),
        ("TCP over WiFi", tcpWifiThroughput_delay),
        ("Aggregate TCP over WiFi and 3G", tcpAggThroughput_delay),
        ("MPTCP", mptcpThroughput_delay)])


# Varies loss rate on the Wifi link
def runLossExp():
    print "Running loss experiment:"
    losses = range(0,100,5)
    tcpWifiThroughput_loss = []
    tcpAggThroughput_loss = []
    mptcpThroughput_loss = []
    for loss in losses:
      # Create topology with given loss rate
      topo = MyTopo(wifiLoss=loss)
      net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink, controller=OVSController)
      net.start()
      begin_mptcp_experiment(net)

      # Do regular TCP throughput measurements
      receiveProcess = start_receiver(net)
      sendProcess = start_sender(net, receiver_wifi_interface_ip, 3600)
      sendProcess = start_sender(net, receiver_3g_interface_ip, 3600)
      sleep(5)
      print "Wifi throughput with loss " + str(loss)
      a = median(get_rates('s0-eth2'))
      print a
      tcpWifiThroughput_loss.append(a)
      print "3G throughput"
      b = median(get_rates('s1-eth2'))
      print b
      print "Aggregate TCP throughput with WiFi loss " + str(loss)
      c = a+b
      print c
      tcpAggThroughput_loss.append(c)

      stop_iperf()

      # Enable MPTCP with optimizations
      set_sysctl('net.mptcp.mptcp_enabled', '1')
      set_sysctl('net.mptcp.mptcp_opportunistic_retransmit', '1')
      set_sysctl('net.mptcp.mptcp_penalize_slow_subflows', '1')

      # Do MPTCP throughput measurements
      collected_throughputs = []
      for i in xrange(0, NUM_TRIES):
        receiveProcess = start_receiver(net)
        sendProcess = start_sender(net, receiver_3g_interface_ip, 3600)
        sleep(warm_up_time_min)
        wifi = median(get_rates('s0-eth2'))
        threeg = median(get_rates('s1-eth2'))
        collected_throughputs.append(wifi + threeg)
        stop_iperf()

      print "MPTCP throughput with WiFi loss " + str(loss)
      print collected_throughputs
      med_throughput = median(collected_throughputs)
      print med_throughput
      mptcpThroughput_loss.append(med_throughput)

      # Disable MPTCP and optimizations
      set_sysctl('net.mptcp.mptcp_enabled', '0')
      set_sysctl('net.mptcp.mptcp_opportunistic_retransmit', '0')
      set_sysctl('net.mptcp.mptcp_penalize_slow_subflows', '0')

      # Stop the network
      net.stop()

    write_csv ("loss_quality.csv", [("WiFi loss rate (%)", losses),
        ("TCP over WiFi", tcpWifiThroughput_loss),
        ("Aggregate TCP over WiFi and 3G", tcpAggThroughput_loss),
        ("MPTCP", mptcpThroughput_loss)])

def main():
    """ Run the paper's MPTCP experiment."""
    start = time()
    # Reset to known state
    topo = MyTopo()
    net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink, controller=OVSController)
    net.start()
    dumpNodeConnections(net.hosts)
    net.pingAll()

    # Set up interface ips
    begin_mptcp_experiment(net)
    # Verify topology
    verify_topology(net)

    # How Hard Can It Be? Designing and implementing a Deplyable Multipath TCP
    # Produce the results of figure 4a
    figure_4a(net)

    # Produce the results of figure 4b
    figure_4b(net)

    # Produce the results of figure 4c
    figure_4c(net)

    net.stop()
    # Shut down iperf processes
    stop_iperf()
    # Restore sysctl to previous state
    reset_sysctl_options()

    """Run the additional quality experiments."""
    runRTTExp()
    runLossExp()

if __name__ == '__main__':
    try:
        main()
    except:
        print "-"*80
        print "Caught exception.  Cleaning up..."
        print "-"*80
        import traceback
        traceback.print_exc()
        reset_sysctl_options()
        os.system("killall -9 top bwm-ng tcpdump cat mnexec iperf; mn -c")
