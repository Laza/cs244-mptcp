Step 1: Launch a c3.large instance using the AMI ami-994033a9

Step 2: git clone git@bitbucket.org:Laza/cs244-mptcp.git

Step 3: sudo ./run.sh (We recommend running the experiments within a utility like Screen since it can take ~10 hours for all experiments to complete)

Step 4: The output .png files contain the various graphs